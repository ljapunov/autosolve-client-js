var rascal = require('rascal')
var Broker = rascal.BrokerAsPromised;
var request = require('request');
const EventEmitter = require('events');
const AutoSolveConstants = require('./AutoSolveConstants');

class InstanceAutoSolveClass {
    constructor(params) {
        this.constants = new AutoSolveConstants();
        this.ee = new EventEmitter.EventEmitter();

        this.accessToken = params.accessToken;
        this.apiKey = params.apiKey;
        this.clientKey = params.clientKey;
        this.debug = params.debug;
        this.shouldLog = params.debug != undefined;
        this.shouldAlertOnCancel = params.shouldAlertOnCancel == null ? false : params.shouldAlertOnCancel;
        this.vhost = this.constants.VHOST
        this.lastConnection = 0;

        this.accountId;
        this.directExchangeName;
        this.fanoutExchangeName;
        this.receiverQueue;
        this.routingKey;
        this.sendRoutingKey;
        this.tokenSendRoutingKey;
        this.cancelSendRoutingKey;
        this.receiverRoutingKey;
        this.cancelRoutingKey;
        this.config
    }

    //Main init. Access token and api key can be null
    async init(accessToken, apiKey){
        return new Promise(async (resolve, reject) => {
            await this.handleResetCredentials(accessToken, apiKey).catch((err) => this.logger(err))

            let statusCode = await this.validateCredentials()
            if(statusCode == 200) {
                this.accountId = this.accessToken.split("-")[0];
                this.setRoutingObjects();
                this.config = this.generateConfig();
                if(isNaN(this.accountId)){
                    reject(this.constants.ERRORS.INVALID_ACCESS_TOKEN)
                    return;
                }

                let delay = this.lastConnection - (this.getEpochSeconds() - this.constants.RETRY_CONNECTION_DELAY)
                delay = delay < 0 ? 0 : (delay * 1000)

                setTimeout(async () => {
                    this.lastConnection = this.getEpochSeconds()
                    const broker = await Broker.create(this.config).catch((response) => { 
                        this.ee.emit(this.constants.RESPONSE_EVENTS.ERROR, this.constants.ERRORS.CONNECTION_ERROR_INIT)
                        reject(response)
                    })

                    this.broker = broker;
                    this.broker.on('error', (err) => this.ee.emit(this.constants.RESPONSE_EVENTS.ERROR, err))
    
                    await this.registerReceiverSubscription().catch(() => {
                        this.ee.emit(this.constants.RESPONSE_EVENTS.ERROR, this.constants.ERRORS.CONNECTION_ERROR_INIT)
                        reject()
                    })
                    
                    resolve()
                }, delay)
            } else {
                statusCode == 400 ? reject(this.constants.ERRORS.INVALID_CLIENT_ID) :
                statusCode == 401 ? reject(this.constants.ERRORS.INVALID_API_OR_ACCESS_TOKEN) :
                statusCode == 429 ? reject(this.constants.ERRORS.TOO_MANY_REQUESTS) :
                    reject(this.constants.ERRORS.UNKNOWN_ERROR)

                return;
            }
        })
    }

    handleResetCredentials(accessToken, apiKey) {
        return new Promise((resolve, reject) => {
            let newApiKey = apiKey == null ? this.apiKey : apiKey;
            let newAccessToken = accessToken == null ? this.accessToken : accessToken;

            resolve(this.resetCredentials(newAccessToken, newApiKey))
        })
    }

    resetCredentials(accessToken, apiKey) {
        return new Promise(async (resolve, reject) => {
            await this.closeConnection().catch((err) => console.log(err))
            this.accessToken = accessToken;
            this.apiKey = apiKey;
            resolve()
        })
    }

    setRoutingObjects() {
        this.routingKey = this.replaceAllDashes(this.apiKey);
        this.sendRoutingKey = this.replaceAllDashes(this.accessToken);

        this.directExchangeName = this.buildWithAccountId(this.constants.DIRECT_EXCHANGE);
        this.fanoutExchangeName = this.buildWithAccountId(this.constants.FANOUT_EXCHANGE);

        this.receiverQueue = this.buildWithCredentials(this.constants.RECEIVER_QUEUE_NAME);
        this.receiverRoutingKey = this.buildWithCredentials(this.constants.TOKEN_RECEIVE_ROUTE);
        this.cancelRoutingKey = this.buildWithCredentials(this.constants.CANCEL_RECEIVE_ROUTE);

        this.tokenSendRoutingKey = this.buildWithAccessToken(this.constants.TOKEN_SEND_ROUTE);
        this.cancelSendRoutingKey = this.buildWithAccessToken(this.constants.CANCEL_SEND_ROUTE);
    }

    async validateCredentials() {
        return new Promise((resolve, reject) => {
            this.logger("Validating credentials")
            request({
                url: `https://dash.autosolve.aycd.io/rest/${this.accessToken}/verify/${this.apiKey}?clientId=${this.clientKey}`,
                method: 'get',
                headers: {
                    'content-type': "application/json"
                }
            }, function(err, res, body) {
                res ? resolve(res.statusCode) : reject()
            })
        })
    }

    //Registers a listener for responses from AutoSolve
    //Sends the response using the preferred callback method (EventEmitter or callback specified in constructor)
    async registerReceiverSubscription() {
        return new Promise(async (resolve, reject) => {
            const subscription = await this.broker.subscribe(this.constants.RASCALKEY.TOKEN_RECEIVE);
            subscription.on('message', (message, content, ackOrNack) => {
                let buffer = Buffer.from(content)
                let jsonMessage = buffer.toString('utf8')
                if(message.fields.routingKey == this.receiverRoutingKey)
                    this.ee.emit(this.constants.RESPONSE_EVENTS.RESPONSE, jsonMessage);
                else
                    this.ee.emit(this.constants.RESPONSE_EVENTS.CANCEL_RESPONSE, jsonMessage);

                ackOrNack();
            }).on('error', console.error);
            resolve();
        })
    }

    //Sends a token request to AutoSolve, where message is in the required AutoSolve format (see testclient.js for example)
    //Returns a boolean response if message successfully published
    sendTokenRequest(message) {
        return new Promise(async (resolve, reject) => {
            if(this.broker != null && this.broker != undefined) {
                message.createdAt = this.getEpochSeconds()
                message.apiKey = this.apiKey;
                this.logger("Sending token request for :: " + message.taskId)
                const publication = await this.broker.publish(this.constants.RASCALKEY.TOKEN_SEND, JSON.stringify(message));
                publication.on('error', (e) => {console.log(`Publication error: ${e}`)})
                resolve(publication)
            } else {
                this.ee.emit(this.constants.RESPONSE_EVENTS.ERROR, "Request not sent, AutoSolve not initialized.");
                reject()
            }
        })
    }

    cancelTokenRequest(taskId) {
        return new Promise(async (resolve, reject) => {
            if(this.broker != null && this.broker != undefined) {
                let message = {"taskId" : taskId, "apiKey" : this.apiKey, "responseRequired" : this.shouldAlertOnCancel}
                message.createdAt = this.getEpochSeconds()
                this.logger("Cancel token request for :: " + message.taskId)
                const publication = await this.broker.publish(this.constants.RASCALKEY.CANCEL_SEND, JSON.stringify(message));
                publication.on('error', (e) => {console.log(`Publication error: ${e}`)})
                resolve(publication)
            } else {
                this.ee.emit(this.constants.RESPONSE_EVENTS.ERROR, "Cancel request not sent, AutoSolve not initialized.")
                reject();
            }
        })
    }

    cancelAllRequests() {
        return new Promise(async (resolve, reject) => {
            if(this.broker != null && this.broker != undefined) {
                let message = {"apiKey" : this.apiKey}
                message.createdAt = this.getEpochSeconds()
                this.logger("Clearing all outstanding requests for :: " + message.apiKey)
                const publication = await this.broker.publish(this.constants.RASCALKEY.CANCEL_SEND, JSON.stringify(message));
                publication.on('error', (e) => {console.log(`Publication error: ${e}`)})
                resolve(publication)
            } else {
                this.ee.emit(this.constants.RESPONSE_EVENTS.ERROR, "Cancel all request, AutoSolve not initialized.")
                reject();
            }
        })
    }

    generateConfig() {
        let definitions = { }
        try {
            definitions["vhosts"] = {}
            definitions["vhosts"][this.vhost] = {}
            definitions["vhosts"][this.vhost]["publications"] = {}
            definitions["vhosts"][this.vhost]["subscriptions"] = {}
            definitions["vhosts"][this.vhost]["exchanges"] = {}
            definitions["vhosts"][this.vhost]["queues"] = {}
            definitions["vhosts"][this.vhost]["connection"] = { 
                "url": `amqp://${this.accountId}:${this.accessToken}@${this.constants.HOSTNAME}:5672/${this.vhost}?heartbeat=10`,
                "retry": {
                    "min": 1000,
                    "max": 30000,
                    "factor": 2,
                    "strategy": "exponential"
                }
            }
            definitions["vhosts"][this.vhost]["exchanges"][this.directExchangeName] = {
                "type" : "direct",
                "assert" : false,
            }
            definitions["vhosts"][this.vhost]["exchanges"][this.fanoutExchangeName] = {
                "type" : "fanout",
                "assert" : false,
            }
            definitions["vhosts"][this.vhost]["queues"][this.receiverQueue] = {
                "assert" : false,
            }   
            definitions["vhosts"][this.vhost]["bindings"] = [
                `${this.directExchangeName}[${this.receiverRoutingKey}] -> ${this.receiverQueue}`,
                `${this.directExchangeName}[${this.cancelRoutingKey}] -> ${this.receiverQueue}`
            ]
            definitions["vhosts"][this.vhost]["publications"][this.constants.RASCALKEY.TOKEN_SEND] = {
                "exchange": this.directExchangeName,
                "routingKey": this.tokenSendRoutingKey
            }
            definitions["vhosts"][this.vhost]["publications"][this.constants.RASCALKEY.CANCEL_SEND] = {
                "exchange": this.fanoutExchangeName,
                "routingKey": this.cancelSendRoutingKey
            }
            definitions["vhosts"][this.vhost]["subscriptions"][this.constants.RASCALKEY.TOKEN_RECEIVE] = {
                "queue" : this.receiverQueue
            }
            definitions["vhosts"][this.vhost]["subscriptions"][this.constants.RASCALKEY.CANCEL_RECEIVE] = {
                "queue" : this.receiverQueue
            }
    
            return rascal.withDefaultConfig(definitions)
        } catch(e) {
            this.ee.emit(this.constants.RESPONSE_EVENTS.ERROR, this.constants.ERRORS.CONNECTION_ERROR_INIT)
        }
    }

    //Generates a routing key to ensure responses get sent to this client
    buildWithCredentials(routingKeyPrefix) {
        return  `${routingKeyPrefix}.${this.accountId}.${this.routingKey}`
    }

    buildWithAccessToken(prefix){
        return `${prefix}.${this.sendRoutingKey}`
    }

    buildWithAccountId(prefix) {
        return `${prefix}.${this.accountId}`
    }

    //Safe close connection, optional
    closeConnection() {
        return new Promise(async (resolve, reject) => {
            if(this.broker != null && this.broker != undefined) {
                await this.broker.shutdown().catch((err) => reject(err))
            }
            resolve();
        })
    }

    //Wrapper for console logging only if params.debug is specified in constructor
    logger(message) {
        if(this.debug) { console.log(message) }
    }

    replaceAllDashes(original) {
        return original.split(/[-_ ]/).join("");
    };

    getEpochSeconds() {
        return Math.round(Date.now() / 1000)
    }
}

var AutoSolveFactory = (function(){
    function CreateInstance(params) {
        let autoSolve = new InstanceAutoSolveClass(params)
        return autoSolve;
    }
    var instance;
    return {
        getInstance: function(params){
            if (instance == null) {
                instance = new CreateInstance(params);
                instance.constructor = null;
            }
            return instance;
        }
   };
})();

module.exports = AutoSolveFactory;