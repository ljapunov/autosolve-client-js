class AutoSolveConstants {
    constructor() {
        //NAMES
        this.HOSTNAME = "amqp.autosolve.aycd.io"
        this.VHOST = "oneclick"
        this.DIRECT_EXCHANGE = "exchanges.direct";
        this.FANOUT_EXCHANGE = "exchanges.fanout";
        this.RECEIVER_QUEUE_NAME = "queues.response.direct";

        //ROUTES
        this.TOKEN_SEND_ROUTE = "routes.request.token";
        this.CANCEL_SEND_ROUTE = "routes.request.token.cancel";
        this.TOKEN_RECEIVE_ROUTE = "routes.response.token";
        this.CANCEL_RECEIVE_ROUTE = "routes.response.token.cancel";

        //CONFIG
        this.RETRY_CONNECTION_DELAY = 5; //seconds
        this.AUTO_DELETE = false;
        this.DURABLE = false;
        this.AUTO_ACK = true;
        this.EXCLUSIVE = false;

        this.CONNECTION_EVENT_KEY = "connectionEvent";

        this.EVENT_TYPES = {
            CONNECTION: "connection",
            CHANNEL: "channel",
            BLOCKED_UNBLOCKED: "blocked_unblocked",
            SUCCESS: "success"
        };

        this.EVENTS = {
            CLOSURE_EVENT: "closed",
            ERROR_EVENT: "error",
            BLOCKED_EVENT: "blocked",
            UNBLOCKED_EVENT: "unblocked"
        };

        this.RESPONSE_EVENTS = {
            RESPONSE : "AutoSolveResponse",
            CANCEL_RESPONSE : "AutoSolveResponse_Cancel",
            ERROR : "AutoSolveError"
        }

        this.ERRORS = {
            INVALID_ACCESS_TOKEN: "Invalid Access Token",
            INVALID_CLIENT_ID: "Invalid Client ID",
            INVALID_API_OR_ACCESS_TOKEN: "Invalid API Key or Access Token",
            TOO_MANY_REQUESTS: "Too many requests are being attempted. Cooling down",
            CONNECTION_ERROR_INIT: "Connection error when initializing. Please retry",
            CONNECTION_ERROR: "Error establishing connection",
            CHANNEL_ERROR: "Error creating channel",
            EXCHANGE_ERROR: "Error asserting rabbit exchange",
            QUEUE_ERROR: "Error asserting queue",
            BINDING_ERROR: "Error binding queue to exchange",
            MESSAGE_RECEIEVE_ERROR: "Error receiving message",
            SEND_TOKEN_REQUEST_ERROR: "Send token request error",
            UNKNOWN_ERROR: "An unknown error occured"
        };

        this.DEFAULT_QUEUE_PARAMS = {
            "x-expires": 180000,
            "autoDelete": false,
            "durable": false
        };

        this.TYPE = {
            TOKEN: "token",
            CANCEL: "cancel"
        };

        this.RASCALKEY = {
            TOKEN_SEND: "token_send",
            CANCEL_SEND: "cancel_send",
            TOKEN_RECEIVE: "token_receive",
            CANCEL_RECEIVE: "cancel_receive"
        };
    }
}

module.exports = AutoSolveConstants