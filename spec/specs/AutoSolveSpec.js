describe("AutoSolve", function() {
  var AutoSolveClient = require('../../lib/autosolve_classes/AutoSolveClient');
  var AutoSolve = require('../../autosolve')
  var autoSolveClient;

  beforeEach(function() {
    autoSolveClient = new AutoSolveClient();
    autoSolve = new AutoSolve({"accessToken" : "testuser-autosolve!", "apiKey" : "e63ccc58-839c-432d-98ce-758828f48a6c", "clientKey" : "myclientkey","debug" : true});
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
  });

  it("should be able to connect to rabbitmq", async function() {
    let result = await autoSolveClient.connect(autoSolve);
    expect(result).toEqual(true);
  });

  describe("when giving incorrect credentials", function() {
    beforeEach(async function() {
      autoSolveClient = new AutoSolveClient();
      autoSolve = new AutoSolve({"accessToken" : "testuser-badpassword", "apiKey" : "e63ccc58-839c-432d-98ce-758828f48a6c", "clientKey" : "myclientkey", "debug" : true});
    });

    it("should be not be able to connect to rabbitmq with incorrect credentials", async function() {
      let result = await autoSolveClient.connect(autoSolve);
      expect(result).toContain("403 (ACCESS-REFUSED)");
    });
  });

  
  describe("after connection is made", function() {
    beforeEach(async function() {
      autoSolveClient = new AutoSolveClient();
      autoSolve = new AutoSolve({"accessToken" : "testuser-autosolve!", "apiKey" : "e63ccc58-839c-432d-98ce-758828f48a6c", "clientKey" : "myclientkey", "debug" : true});
    });

    it("it should be able to send a message", async function() {
      let exampleMessage = {
        "taskId" : "task1", 
        "url" : "https://example.com", 
        "siteKey" : "recaptchaSiteKey", 
        "apiKey" : autoSolve.apiKey, 
        "version" : "version", 
        "action" : "verify", 
        "minScore" : 0.7, 
        "proxy" : "example.com:80:user:password", 
        "userAgent" : "Mozilla/5.0 (iPhone9,4; U; CPU iPhone OS 10_0_1 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) Version/10.0 Mobile/14A403 Safari/602.1",
        "cookies" : "cookie1=cookie,cookie2=cookie2"
      }
      let result = await autoSolveClient.sendMessage(autoSolve, exampleMessage)
      expect(result).toEqual(true);
    });

    it("it should be able to send and receive a message", async function() {
      let exampleMessage = {
        "taskId" : "task1", 
        "url" : "https://example.com", 
        "siteKey" : "recaptchaSiteKey", 
        "apiKey" : autoSolve.apiKey, 
        "version" : "version", 
        "action" : "verify", 
        "minScore" : 0.7, 
        "proxy" : "example.com:80:user:password", 
        "userAgent" : "Mozilla/5.0 (iPhone9,4; U; CPU iPhone OS 10_0_1 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) Version/10.0 Mobile/14A403 Safari/602.1",
        "cookies" : "cookie1=cookie,cookie2=cookie2"
      }
      let result = await autoSolveClient.sendAndRecieveMessage(autoSolve, exampleMessage)
      let jsonObj = JSON.parse(result)
      expect(jsonObj.taskId).toEqual("task1");
      expect(jsonObj.apiKey).toEqual(autoSolve.apiKey);
    });

    it("it should fail and catch error if connection cut before send", async function() {
      let exampleMessage = {
        "apiKey" : autoSolve.apiKey
      }

      let result = await autoSolveClient.sendMessageAfterClose(autoSolve, exampleMessage)

      expect(result.connectionCloseResult).toEqual(true)
      expect(result.tokenSendResult).toEqual(autoSolve.constants.ERRORS.SEND_TOKEN_REQUEST_ERROR);
    });
  });


  describe("after connection is made to OneClick", function() {
    beforeEach(async function() {
      autoSolveClient = new AutoSolveClient();
      autoSolve = new AutoSolve({"username" : "testuser", "password" : "TestUserAycd1!", "apiKey" : "myapikey", "clientKey" : "myclientkey"});
    });
  });
});
