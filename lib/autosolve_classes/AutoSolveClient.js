function AutoSolveClient() {}

AutoSolveClient.prototype.connect = async function(autoSolve) {
  return new Promise(async (resolve, reject) => {
    await autoSolve.init().then(() => {
      resolve(true);
    }).catch((err) => {
      resolve(err)
    })
  })
};

AutoSolveClient.prototype.sendMessage = async function(autoSolve, message) {
  return new Promise(async (resolve, reject) => {
    await autoSolve.init().then(() => {
      let result = autoSolve.sendTokenRequestTesting(message, `${autoSolve.routingKey}.${autoSolve.constants.RECEIVER}`)
      resolve(result)
    }).catch((err) => {
      resolve(err)
    })
  })
};

AutoSolveClient.prototype.sendAndRecieveMessage = async function(autoSolve, message) {
  return new Promise(async (resolve, reject) => {
    await autoSolve.init().then(() => {
      autoSolve.ee.on(`AutosolveResponse_${message.taskId}`, (data) => {
        resolve(data)
      })

      autoSolve.sendTokenRequestTesting(message, `${autoSolve.routingKey}.${autoSolve.constants.RECEIVER}`)
    }).catch((err) => {
      resolve(err)
    })
  })
};


AutoSolveClient.prototype.sendMessageAfterClose = async function(autoSolve, message) {
  return new Promise(async (resolve, reject) => {
    await autoSolve.init().then(() => {
      let connectionCloseResult = autoSolve.closeConnection()
      autoSolve.sendTokenRequestTesting(message, `${message.apiKey}.${autoSolve.constants.RECEIVER}`).then(tokenSendResult => {
        resolve({"connectionCloseResult" : connectionCloseResult, "tokenSendResult" : tokenSendResult});
      })
    })
  })
};

module.exports = AutoSolveClient;
