'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var rascal = require('rascal');
var Broker = rascal.BrokerAsPromised;
var request = require('request');
var EventEmitter = require('events');
var AutoSolveConstants = require('./AutoSolveConstants');

var InstanceAutoSolveClass = function () {
    function InstanceAutoSolveClass(params) {
        _classCallCheck(this, InstanceAutoSolveClass);

        this.constants = new AutoSolveConstants();
        this.ee = new EventEmitter.EventEmitter();

        this.accessToken = params.accessToken;
        this.apiKey = params.apiKey;
        this.clientKey = params.clientKey;
        this.debug = params.debug;
        this.shouldLog = params.debug != undefined;
        this.shouldAlertOnCancel = params.shouldAlertOnCancel == null ? false : params.shouldAlertOnCancel;
        this.vhost = this.constants.VHOST;
        this.lastConnection = 0;

        this.accountId;
        this.directExchangeName;
        this.fanoutExchangeName;
        this.receiverQueue;
        this.routingKey;
        this.sendRoutingKey;
        this.tokenSendRoutingKey;
        this.cancelSendRoutingKey;
        this.receiverRoutingKey;
        this.cancelRoutingKey;
        this.config;
    }

    //Main init. Access token and api key can be null


    _createClass(InstanceAutoSolveClass, [{
        key: 'init',
        value: function () {
            var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(accessToken, apiKey) {
                var _this = this;

                return regeneratorRuntime.wrap(function _callee3$(_context3) {
                    while (1) {
                        switch (_context3.prev = _context3.next) {
                            case 0:
                                return _context3.abrupt('return', new Promise(function () {
                                    var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(resolve, reject) {
                                        var statusCode, delay;
                                        return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                            while (1) {
                                                switch (_context2.prev = _context2.next) {
                                                    case 0:
                                                        _context2.next = 2;
                                                        return _this.handleResetCredentials(accessToken, apiKey).catch(function (err) {
                                                            return _this.logger(err);
                                                        });

                                                    case 2:
                                                        _context2.next = 4;
                                                        return _this.validateCredentials();

                                                    case 4:
                                                        statusCode = _context2.sent;

                                                        if (!(statusCode == 200)) {
                                                            _context2.next = 17;
                                                            break;
                                                        }

                                                        _this.accountId = _this.accessToken.split("-")[0];
                                                        _this.setRoutingObjects();
                                                        _this.config = _this.generateConfig();

                                                        if (!isNaN(_this.accountId)) {
                                                            _context2.next = 12;
                                                            break;
                                                        }

                                                        reject(_this.constants.ERRORS.INVALID_ACCESS_TOKEN);
                                                        return _context2.abrupt('return');

                                                    case 12:
                                                        delay = _this.lastConnection - (_this.getEpochSeconds() - _this.constants.RETRY_CONNECTION_DELAY);

                                                        delay = delay < 0 ? 0 : delay * 1000;

                                                        setTimeout(_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                                                            var broker;
                                                            return regeneratorRuntime.wrap(function _callee$(_context) {
                                                                while (1) {
                                                                    switch (_context.prev = _context.next) {
                                                                        case 0:
                                                                            _this.lastConnection = _this.getEpochSeconds();
                                                                            _context.next = 3;
                                                                            return Broker.create(_this.config).catch(function (response) {
                                                                                _this.ee.emit(_this.constants.RESPONSE_EVENTS.ERROR, _this.constants.ERRORS.CONNECTION_ERROR_INIT);
                                                                                reject(response);
                                                                            });

                                                                        case 3:
                                                                            broker = _context.sent;


                                                                            _this.broker = broker;
                                                                            _this.broker.on('error', function (err) {
                                                                                return _this.ee.emit(_this.constants.RESPONSE_EVENTS.ERROR, err);
                                                                            });

                                                                            _context.next = 8;
                                                                            return _this.registerReceiverSubscription().catch(function () {
                                                                                _this.ee.emit(_this.constants.RESPONSE_EVENTS.ERROR, _this.constants.ERRORS.CONNECTION_ERROR_INIT);
                                                                                reject();
                                                                            });

                                                                        case 8:

                                                                            resolve();

                                                                        case 9:
                                                                        case 'end':
                                                                            return _context.stop();
                                                                    }
                                                                }
                                                            }, _callee, _this);
                                                        })), delay);
                                                        _context2.next = 19;
                                                        break;

                                                    case 17:
                                                        statusCode == 400 ? reject(_this.constants.ERRORS.INVALID_CLIENT_ID) : statusCode == 401 ? reject(_this.constants.ERRORS.INVALID_API_OR_ACCESS_TOKEN) : statusCode == 429 ? reject(_this.constants.ERRORS.TOO_MANY_REQUESTS) : reject(_this.constants.ERRORS.UNKNOWN_ERROR);

                                                        return _context2.abrupt('return');

                                                    case 19:
                                                    case 'end':
                                                        return _context2.stop();
                                                }
                                            }
                                        }, _callee2, _this);
                                    }));

                                    return function (_x3, _x4) {
                                        return _ref2.apply(this, arguments);
                                    };
                                }()));

                            case 1:
                            case 'end':
                                return _context3.stop();
                        }
                    }
                }, _callee3, this);
            }));

            function init(_x, _x2) {
                return _ref.apply(this, arguments);
            }

            return init;
        }()
    }, {
        key: 'handleResetCredentials',
        value: function handleResetCredentials(accessToken, apiKey) {
            var _this2 = this;

            return new Promise(function (resolve, reject) {
                var newApiKey = apiKey == null ? _this2.apiKey : apiKey;
                var newAccessToken = accessToken == null ? _this2.accessToken : accessToken;

                resolve(_this2.resetCredentials(newAccessToken, newApiKey));
            });
        }
    }, {
        key: 'resetCredentials',
        value: function resetCredentials(accessToken, apiKey) {
            var _this3 = this;

            return new Promise(function () {
                var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(resolve, reject) {
                    return regeneratorRuntime.wrap(function _callee4$(_context4) {
                        while (1) {
                            switch (_context4.prev = _context4.next) {
                                case 0:
                                    _context4.next = 2;
                                    return _this3.closeConnection().catch(function (err) {
                                        return console.log(err);
                                    });

                                case 2:
                                    _this3.accessToken = accessToken;
                                    _this3.apiKey = apiKey;
                                    resolve();

                                case 5:
                                case 'end':
                                    return _context4.stop();
                            }
                        }
                    }, _callee4, _this3);
                }));

                return function (_x5, _x6) {
                    return _ref4.apply(this, arguments);
                };
            }());
        }
    }, {
        key: 'setRoutingObjects',
        value: function setRoutingObjects() {
            this.routingKey = this.replaceAllDashes(this.apiKey);
            this.sendRoutingKey = this.replaceAllDashes(this.accessToken);

            this.directExchangeName = this.buildWithAccountId(this.constants.DIRECT_EXCHANGE);
            this.fanoutExchangeName = this.buildWithAccountId(this.constants.FANOUT_EXCHANGE);

            this.receiverQueue = this.buildWithCredentials(this.constants.RECEIVER_QUEUE_NAME);
            this.receiverRoutingKey = this.buildWithCredentials(this.constants.TOKEN_RECEIVE_ROUTE);
            this.cancelRoutingKey = this.buildWithCredentials(this.constants.CANCEL_RECEIVE_ROUTE);

            this.tokenSendRoutingKey = this.buildWithAccessToken(this.constants.TOKEN_SEND_ROUTE);
            this.cancelSendRoutingKey = this.buildWithAccessToken(this.constants.CANCEL_SEND_ROUTE);
        }
    }, {
        key: 'validateCredentials',
        value: function () {
            var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                var _this4 = this;

                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                    while (1) {
                        switch (_context5.prev = _context5.next) {
                            case 0:
                                return _context5.abrupt('return', new Promise(function (resolve, reject) {
                                    _this4.logger("Validating credentials");
                                    request({
                                        url: 'https://dash.autosolve.aycd.io/rest/' + _this4.accessToken + '/verify/' + _this4.apiKey + '?clientId=' + _this4.clientKey,
                                        method: 'get',
                                        headers: {
                                            'content-type': "application/json"
                                        }
                                    }, function (err, res, body) {
                                        res ? resolve(res.statusCode) : reject();
                                    });
                                }));

                            case 1:
                            case 'end':
                                return _context5.stop();
                        }
                    }
                }, _callee5, this);
            }));

            function validateCredentials() {
                return _ref5.apply(this, arguments);
            }

            return validateCredentials;
        }()

        //Registers a listener for responses from AutoSolve
        //Sends the response using the preferred callback method (EventEmitter or callback specified in constructor)

    }, {
        key: 'registerReceiverSubscription',
        value: function () {
            var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
                var _this5 = this;

                return regeneratorRuntime.wrap(function _callee7$(_context7) {
                    while (1) {
                        switch (_context7.prev = _context7.next) {
                            case 0:
                                return _context7.abrupt('return', new Promise(function () {
                                    var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(resolve, reject) {
                                        var subscription;
                                        return regeneratorRuntime.wrap(function _callee6$(_context6) {
                                            while (1) {
                                                switch (_context6.prev = _context6.next) {
                                                    case 0:
                                                        _context6.next = 2;
                                                        return _this5.broker.subscribe(_this5.constants.RASCALKEY.TOKEN_RECEIVE);

                                                    case 2:
                                                        subscription = _context6.sent;

                                                        subscription.on('message', function (message, content, ackOrNack) {
                                                            var buffer = Buffer.from(content);
                                                            var jsonMessage = buffer.toString('utf8');
                                                            if (message.fields.routingKey == _this5.receiverRoutingKey) _this5.ee.emit(_this5.constants.RESPONSE_EVENTS.RESPONSE, jsonMessage);else _this5.ee.emit(_this5.constants.RESPONSE_EVENTS.CANCEL_RESPONSE, jsonMessage);

                                                            ackOrNack();
                                                        }).on('error', console.error);
                                                        resolve();

                                                    case 5:
                                                    case 'end':
                                                        return _context6.stop();
                                                }
                                            }
                                        }, _callee6, _this5);
                                    }));

                                    return function (_x7, _x8) {
                                        return _ref7.apply(this, arguments);
                                    };
                                }()));

                            case 1:
                            case 'end':
                                return _context7.stop();
                        }
                    }
                }, _callee7, this);
            }));

            function registerReceiverSubscription() {
                return _ref6.apply(this, arguments);
            }

            return registerReceiverSubscription;
        }()

        //Sends a token request to AutoSolve, where message is in the required AutoSolve format (see testclient.js for example)
        //Returns a boolean response if message successfully published

    }, {
        key: 'sendTokenRequest',
        value: function sendTokenRequest(message) {
            var _this6 = this;

            return new Promise(function () {
                var _ref8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(resolve, reject) {
                    var publication;
                    return regeneratorRuntime.wrap(function _callee8$(_context8) {
                        while (1) {
                            switch (_context8.prev = _context8.next) {
                                case 0:
                                    if (!(_this6.broker != null && _this6.broker != undefined)) {
                                        _context8.next = 11;
                                        break;
                                    }

                                    message.createdAt = _this6.getEpochSeconds();
                                    message.apiKey = _this6.apiKey;
                                    _this6.logger("Sending token request for :: " + message.taskId);
                                    _context8.next = 6;
                                    return _this6.broker.publish(_this6.constants.RASCALKEY.TOKEN_SEND, JSON.stringify(message));

                                case 6:
                                    publication = _context8.sent;

                                    publication.on('error', function (e) {
                                        console.log('Publication error: ' + e);
                                    });
                                    resolve(publication);
                                    _context8.next = 13;
                                    break;

                                case 11:
                                    _this6.ee.emit(_this6.constants.RESPONSE_EVENTS.ERROR, "Request not sent, AutoSolve not initialized.");
                                    reject();

                                case 13:
                                case 'end':
                                    return _context8.stop();
                            }
                        }
                    }, _callee8, _this6);
                }));

                return function (_x9, _x10) {
                    return _ref8.apply(this, arguments);
                };
            }());
        }
    }, {
        key: 'cancelTokenRequest',
        value: function cancelTokenRequest(taskId) {
            var _this7 = this;

            return new Promise(function () {
                var _ref9 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(resolve, reject) {
                    var message, publication;
                    return regeneratorRuntime.wrap(function _callee9$(_context9) {
                        while (1) {
                            switch (_context9.prev = _context9.next) {
                                case 0:
                                    if (!(_this7.broker != null && _this7.broker != undefined)) {
                                        _context9.next = 11;
                                        break;
                                    }

                                    message = { "taskId": taskId, "apiKey": _this7.apiKey, "responseRequired": _this7.shouldAlertOnCancel };

                                    message.createdAt = _this7.getEpochSeconds();
                                    _this7.logger("Cancel token request for :: " + message.taskId);
                                    _context9.next = 6;
                                    return _this7.broker.publish(_this7.constants.RASCALKEY.CANCEL_SEND, JSON.stringify(message));

                                case 6:
                                    publication = _context9.sent;

                                    publication.on('error', function (e) {
                                        console.log('Publication error: ' + e);
                                    });
                                    resolve(publication);
                                    _context9.next = 13;
                                    break;

                                case 11:
                                    _this7.ee.emit(_this7.constants.RESPONSE_EVENTS.ERROR, "Cancel request not sent, AutoSolve not initialized.");
                                    reject();

                                case 13:
                                case 'end':
                                    return _context9.stop();
                            }
                        }
                    }, _callee9, _this7);
                }));

                return function (_x11, _x12) {
                    return _ref9.apply(this, arguments);
                };
            }());
        }
    }, {
        key: 'cancelAllRequests',
        value: function cancelAllRequests() {
            var _this8 = this;

            return new Promise(function () {
                var _ref10 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee10(resolve, reject) {
                    var message, publication;
                    return regeneratorRuntime.wrap(function _callee10$(_context10) {
                        while (1) {
                            switch (_context10.prev = _context10.next) {
                                case 0:
                                    if (!(_this8.broker != null && _this8.broker != undefined)) {
                                        _context10.next = 11;
                                        break;
                                    }

                                    message = { "apiKey": _this8.apiKey };

                                    message.createdAt = _this8.getEpochSeconds();
                                    _this8.logger("Clearing all outstanding requests for :: " + message.apiKey);
                                    _context10.next = 6;
                                    return _this8.broker.publish(_this8.constants.RASCALKEY.CANCEL_SEND, JSON.stringify(message));

                                case 6:
                                    publication = _context10.sent;

                                    publication.on('error', function (e) {
                                        console.log('Publication error: ' + e);
                                    });
                                    resolve(publication);
                                    _context10.next = 13;
                                    break;

                                case 11:
                                    _this8.ee.emit(_this8.constants.RESPONSE_EVENTS.ERROR, "Cancel all request, AutoSolve not initialized.");
                                    reject();

                                case 13:
                                case 'end':
                                    return _context10.stop();
                            }
                        }
                    }, _callee10, _this8);
                }));

                return function (_x13, _x14) {
                    return _ref10.apply(this, arguments);
                };
            }());
        }
    }, {
        key: 'generateConfig',
        value: function generateConfig() {
            var definitions = {};
            try {
                definitions["vhosts"] = {};
                definitions["vhosts"][this.vhost] = {};
                definitions["vhosts"][this.vhost]["publications"] = {};
                definitions["vhosts"][this.vhost]["subscriptions"] = {};
                definitions["vhosts"][this.vhost]["exchanges"] = {};
                definitions["vhosts"][this.vhost]["queues"] = {};
                definitions["vhosts"][this.vhost]["connection"] = {
                    "url": 'amqp://' + this.accountId + ':' + this.accessToken + '@' + this.constants.HOSTNAME + ':5672/' + this.vhost,
                    "retry": {
                        "min": 1000,
                        "max": 30000,
                        "factor": 2,
                        "strategy": "exponential"
                    }
                };
                definitions["vhosts"][this.vhost]["exchanges"][this.directExchangeName] = {
                    "type": "direct",
                    "assert": false
                };
                definitions["vhosts"][this.vhost]["exchanges"][this.fanoutExchangeName] = {
                    "type": "fanout",
                    "assert": false
                };
                definitions["vhosts"][this.vhost]["queues"][this.receiverQueue] = {
                    "assert": false
                };
                definitions["vhosts"][this.vhost]["bindings"] = [this.directExchangeName + '[' + this.receiverRoutingKey + '] -> ' + this.receiverQueue, this.directExchangeName + '[' + this.cancelRoutingKey + '] -> ' + this.receiverQueue];
                definitions["vhosts"][this.vhost]["publications"][this.constants.RASCALKEY.TOKEN_SEND] = {
                    "exchange": this.directExchangeName,
                    "routingKey": this.tokenSendRoutingKey
                };
                definitions["vhosts"][this.vhost]["publications"][this.constants.RASCALKEY.CANCEL_SEND] = {
                    "exchange": this.fanoutExchangeName,
                    "routingKey": this.cancelSendRoutingKey
                };
                definitions["vhosts"][this.vhost]["subscriptions"][this.constants.RASCALKEY.TOKEN_RECEIVE] = {
                    "queue": this.receiverQueue
                };
                definitions["vhosts"][this.vhost]["subscriptions"][this.constants.RASCALKEY.CANCEL_RECEIVE] = {
                    "queue": this.receiverQueue
                };

                return rascal.withDefaultConfig(definitions);
            } catch (e) {
                this.ee.emit(this.constants.RESPONSE_EVENTS.ERROR, this.constants.ERRORS.CONNECTION_ERROR_INIT);
            }
        }

        //Generates a routing key to ensure responses get sent to this client

    }, {
        key: 'buildWithCredentials',
        value: function buildWithCredentials(routingKeyPrefix) {
            return routingKeyPrefix + '.' + this.accountId + '.' + this.routingKey;
        }
    }, {
        key: 'buildWithAccessToken',
        value: function buildWithAccessToken(prefix) {
            return prefix + '.' + this.sendRoutingKey;
        }
    }, {
        key: 'buildWithAccountId',
        value: function buildWithAccountId(prefix) {
            return prefix + '.' + this.accountId;
        }

        //Safe close connection, optional

    }, {
        key: 'closeConnection',
        value: function closeConnection() {
            var _this9 = this;

            return new Promise(function () {
                var _ref11 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee11(resolve, reject) {
                    return regeneratorRuntime.wrap(function _callee11$(_context11) {
                        while (1) {
                            switch (_context11.prev = _context11.next) {
                                case 0:
                                    if (!(_this9.broker != null && _this9.broker != undefined)) {
                                        _context11.next = 3;
                                        break;
                                    }

                                    _context11.next = 3;
                                    return _this9.broker.shutdown().catch(function (err) {
                                        return reject(err);
                                    });

                                case 3:
                                    resolve();

                                case 4:
                                case 'end':
                                    return _context11.stop();
                            }
                        }
                    }, _callee11, _this9);
                }));

                return function (_x15, _x16) {
                    return _ref11.apply(this, arguments);
                };
            }());
        }

        //Wrapper for console logging only if params.debug is specified in constructor

    }, {
        key: 'logger',
        value: function logger(message) {
            if (this.debug) {
                console.log(message);
            }
        }
    }, {
        key: 'replaceAllDashes',
        value: function replaceAllDashes(original) {
            return original.split(/[-_ ]/).join("");
        }
    }, {
        key: 'getEpochSeconds',
        value: function getEpochSeconds() {
            return Math.round(Date.now() / 1000);
        }
    }]);

    return InstanceAutoSolveClass;
}();

var AutoSolveFactory = function () {
    function CreateInstance(params) {
        var autoSolve = new InstanceAutoSolveClass(params);
        return autoSolve;
    }
    var instance;
    return {
        getInstance: function getInstance(params) {
            if (instance == null) {
                instance = new CreateInstance(params);
                instance.constructor = null;
            }
            return instance;
        }
    };
}();

module.exports = AutoSolveFactory;