var AutoSolve = require("./src/autosolve");
const ignite = require("./ignite.js");

let runTests = async() => {
    //let autoSolve = new AutoSolve (;

    let autoSolve = AutoSolve.getInstance(ignite);

    //This is an example of the required format for AutoSolve to handle a captcha token request
    let exampleMessage = {
        //An ID for the task that got hit with a captcha. This can be whatever you want.
        taskId: "task1",

        //Url of the site which the captcha was received
        url: "https://recaptcha.autosolve.io/version/1",

        //Public ReCaptcha key for a given site
        siteKey: "6Ld_LMAUAAAAAOIqLSy5XY9-DUKLkAgiDpqtTJ9b",

        //Api Key your customer needs to fetch fom AYCD Autosolve and input in your application
        //"apiKey" : autoSolve.apiKey,

        //Map object for parameters for ReCaptcha v2, in the grecaptcha.render method
        //"renderParameters" : renderMap,

        //Version of ReCaptcha
        //Options:
        /**
                                V2_CHECKBOX is 0
                                V2_INVISIBLE is 1
                                V3_SCORE is 2
                             */
        version: 0,

        //ReCaptcha action type
        //"action" : "",

        //Minimum score required to pass the recaptcha
        minScore: 0,

        //Proxy used in the task which got the captcha
        //"proxy" : "",

        //User agent used in the request (optional)
        //"userAgent" : "",

        //Cookies in the request (optional)
        //"cookies" : ""
    };

    //Init function for autosolve. register any listeners (if using events callback) in the .then block
    await autoSolve
        .init()
        .then(async() => {
            console.log("Initialized");
            autoSolve.ee.on("AutoSolveResponse", (message) => {
                console.log("Response!");
                let jsonmessage = JSON.parse(message);
                console.log(jsonmessage);
                console.log(jsonmessage.token);
            });

            autoSolve.ee.on("AutoSolveResponse_Cancel", (message) => {
                console.log("Cancelled!");
                let jsonmessage = JSON.parse(message);
                console.log(jsonmessage);
            });

            autoSolve.ee.on("AutoSolveError", (error) => {
                console.log("Error Occurred.");
            });

            autoSolve.sendTokenRequest(exampleMessage);
        })
        .catch((err) => {
            console.log("Error Initializing");
            console.log(err);
        });
};

runTests();